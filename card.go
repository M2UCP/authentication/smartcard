package card

import (
	"errors"
	"fmt"
	"github.com/sf1/go-card/smartcard"
	"strconv"
)

type CardMode int
const(
	CardModeIssuer = CardMode( iota )
	CardModeUser
)

var(
	// read command
	ReadCommand = [ ]byte{ 0x80, 0xBE, 0x00 }

	// update command
	UpdateCommand = [ ]byte{ 0x80, 0xDE, 0x00 }

	// verify command
	// [ i ] -> CSi
	VerifyCommand = [ 3 ][ ]byte{
		{ 0x00, 0x20, 0x00, 0x07, 0x04 },
		{ 0x00, 0x20, 0x00, 0x39, 0x04 },
		{ 0x00, 0x20, 0x00, 0x3B, 0x04 },
	}
)

type Card struct {
	// card
	card *smartcard.Card

	// reader name
	readerName string

	// is alive?
	isAlive bool

	// mode
	mode CardMode

	// manufacturer
	manufacturer [ ]byte
}

// build card
func BuildCard( smartCard *smartcard.Card,
	readerName string ) ( *Card, error ) {
	// allocate
	card := &Card{
		card: smartCard,
		readerName: readerName,
		isAlive: true,
	}

	// read card manufacturer
	if data, err := card.SendRead( 0x00,
		0x04 ); err == nil {
		fmt.Println( "[SMARTCARD] manufacturer:",
			data )
	} else {
		return nil, err
	}

	// read card mode
	if data, err := card.SendRead( 0x04,
		0x04 ); err == nil {
		fmt.Print( "[SMARTCARD] mode: ", data[ 0 ] )
		if data[ 0 ] == 0x40 {
			card.mode = CardModeIssuer
			fmt.Println( " (issuer)")
		} else {
			card.mode = CardModeUser
			fmt.Println( " (user)" )
		}
	} else {
		return nil, err
	}

	// test pin
	if err := card.TypePin( 0,
		[ ]byte{ 0xAA, 0xAA, 0xAA, 0xAA } ); err != nil {
		fmt.Println( "[SMARTCARD]",
			err )
	}

	// done
	return card, nil
}

// close card
func ( card *Card ) Close( ) {
	card.isAlive = false
	_ = card.card.Disconnect( )
}

// read data from card
func ( card *Card ) SendCommand( command smartcard.CommandAPDU,
	mode CommandMode ) ( [ ]byte, error ) {
	// notify
	fmt.Println( "[SMARTCARD] sending command",
		command,
		"to card into \"" +
		card.readerName +
		"\"" )
	// send command
	if response, err := card.card.TransmitAPDU( command ); err == nil {
		fmt.Println( "[SMARTCARD] got response",
			response,
			//"(\"" +
			//	string( response.Data( ) ) +
			"from \"" +//"\") from \"" +
			card.readerName +
			"\"" )
		if err := GenerateError( response.SW1( ),
			response.SW2( ),
			mode ); err == nil {
			data := response.Data( )
			for left, right := 0, len(data)-1; left < right; left, right = left+1, right-1 {
				data[left], data[right] = data[right], data[left]
			}
			return response.Data(), nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}

// get reader name
func ( card *Card ) GetReaderName( ) string {
	return card.readerName
}

// is card alive?
func ( card *Card ) IsAlive( ) bool {
	return card.isAlive
}

// send read command
func ( card *Card ) SendRead( address int,
	length int ) ( [ ]byte, error ) {
	// check length
	if length <= 0 ||
		length % 4 != 0 {
		return nil, errors.New( "bad read length (length % 4 != 0)")
	}

	// read
	output := make( [ ]byte,
		0,
		1 )
	for i := 0; i < length; i += 4 {
		// build apdu
		apdu := ReadCommand
		apdu = append(apdu,
			byte(address + i / 4))
		apdu = append(apdu,
			4)

		// send command
		if readData, err := card.SendCommand(smartcard.CommandAPDU(apdu),
			CommandModeRead); err == nil {
			output = append( output,
				readData... )
		} else {
			return nil, err
		}
	}

	// done
	return output, nil
}

// send update command
func ( card *Card ) SendUpdate( address int,
	data [ ]byte ) error {
	// notify
	fmt.Print( "[SMARTCARD] asked to write \"",
		data,
		"(\"" +
		string( data ),
		"\") on card inside \"" +
		card.readerName +
		"\" from ",
		address,
		"to",
		address + len( data ) )

	// check length
	if len( data ) <= 0 ||
		len( data ) % 4 != 0 {
		fmt.Println( "which is incorrect (not % 4)" )
		return errors.New( "incorrect write length" )
	} else {
		fmt.Println( "" )
	}

	// write data by group of 4
	for i := 0; i < len( data ); i += 4 {
		// buffer
		buffer := data[ i : i + 4 ]

		// reverse buffer
		for left, right := 0, len(buffer)-1; left < right; left, right = left+1, right-1 {
			buffer[left], buffer[right] = buffer[right], buffer[left]
		}

		// build data
		apdu := append( UpdateCommand,
			byte( address + i / 4 ) )
		apdu = append( apdu,
			4 )
		apdu = append( apdu,
			buffer... )

		// build apdu
		if smartcard.CommandAPDU( apdu ).IsValid( ) {
			// send command
			if _, err := card.SendCommand( smartcard.CommandAPDU( apdu ),
				CommandModeUpdate ); err != nil {
				return err
			}
		} else {
			fmt.Println( "[SMARTCARD] splitted write failed because (incorrect apdu)" )
			return errors.New( "incorrect apdu" )
		}
	}
	// notify
	fmt.Println( "[SMARTCARD] finished to write \"",
		data,
		"\" (\"" +
			string( data ) +
		"\") from ",
		address,
		" to ",
		address + len( data ) )

	// done
	return nil
}

// type pin (4 bytes)
// CS is pin number (0->2)
// returns if pin was accepted
func ( card *Card ) TypePin( CS int,
	pin [ ]byte ) error {
	// reverse pin
	for left, right := 0, len(pin)-1; left < right; left, right = left+1, right-1 {
		pin[left], pin[right] = pin[right], pin[left]
	}

	// send command
	if _, err := card.SendCommand( smartcard.CommandAPDU( append( VerifyCommand[ CS ],
		pin... ) ),
		CommandModeVerify ); err == nil {
		fmt.Println( "[SMARTCARD] pin for CS" +
			strconv.Itoa( CS ),
			"on reader \"" +
				card.readerName +
			"\" was accepted" )
		return nil
	} else {
		return err
	}
}
