Smartcard handler
=================

Introduction
------------

This library provides a way to communicate with PC/SC smartcard.

The handler gives you access to all connected card, and to the
last one that was connected, a way to identify your card reader
if you have more than one.

Dependencies
------------

On linux:

```bash
sudo apt-get install pcscd libccid
```

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/M2UCP/authentication/smartcard.git
