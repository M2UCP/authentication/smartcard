package card

import (
	"fmt"
	"github.com/sf1/go-card/smartcard"
	"sync"
	"time"
)

type ReaderDetail struct {
	// reader
	reader *smartcard.Reader

	// last contact time
	lastContact time.Time
}

type Handler struct {
	// smart card context
	context *smartcard.Context

	// reader list
	readerList map[ string ] *ReaderDetail

	// card list
	cardList map[ string ] *Card

	// last added card
	lastAddedCard string

	// mutex
	sync.Mutex
}

// build handler
func BuildHandler( ) ( *Handler, error ) {
	// allocate
	handler := &Handler{
		readerList: make( map[ string ] *ReaderDetail ),
		cardList: make( map[ string ] *Card ),
	}

	// get context
	if context, err := smartcard.EstablishContext( ); err == nil {
		handler.context = context
	} else {
		return nil, err
	}

	// done
	return handler, nil
}

// close handler
func ( handler *Handler ) Close( ) {
	// release context
	_ = handler.context.Release( )
}

// update card reader state
func ( handler *Handler ) Update( ) {
	// lock
	handler.Lock( )
	defer handler.Unlock( )
	
	// look for existing readers
	if readerList, err := handler.context.ListReaders(); err == nil {
		for _, reader := range readerList {
			if readerDetail, ok := handler.readerList[ reader.Name( ) ]; ok {
				readerDetail.lastContact = time.Now( )
				readerDetail.reader = reader
			} else {
				handler.readerList[ reader.Name( ) ] = &ReaderDetail{
					reader:      reader,
					lastContact: time.Now(),
				}
				fmt.Println( "[SMARTCARD] found card reader",
					reader.Name( ) )
			}
		}
	}

	// check existing readers
	for readerName, reader := range handler.readerList {
		if time.Now( ).Sub( reader.lastContact ) >= time.Second * 3 {
			// notify
			fmt.Println( "[SMARTCARD] discarding card reader",
				reader.reader.Name( ),
				"being inactive" )

			// remove from map
			delete( handler.readerList,
				readerName )
		}
	}

	// check existing card
	for readerName, card := range handler.cardList {
		// card still exists but reader is dead?
		if _, ok := handler.readerList[ readerName ]; !ok {
			// notify
			fmt.Println( "[SMARTCARD] discard card on reader",
				readerName,
				"as reader is dead" )

			// close
			card.Close( )

			// delete from map
			delete( handler.cardList,
				readerName )
		}
	}

	// connect cards
	for readerName, reader := range handler.readerList {
		// do we have card in reader?
		if reader.reader.IsCardPresent( ) {
			// is card not already connected?
			if _, ok := handler.cardList[ readerName ]; !ok {
				// connect card
				if card, err := reader.reader.Connect(); err == nil {
					// notify
					fmt.Println("[SMARTCARD] now connected to card on reader",
						readerName)

					// add card
					if newCard, err := BuildCard( card,
						readerName ); err != nil {
						// notify
						fmt.Println( "[SMARTCARD] failed to build card on",
							readerName,
							":",
							err )
					} else {
						// add card
						handler.cardList[ readerName ] = newCard

						// save last added card
						handler.lastAddedCard = readerName
					}
				} else {
					// notify
					fmt.Println("[SMARTCARD] couldn't connect to card on reader",
						readerName,
						":",
						err)
				}
			}
		// do we do NOT have card in reader?
		} else {
			// is card still connected?
			if card, ok := handler.cardList[ readerName ]; ok {
				// kill card
				card.Close( )

				// notify
				fmt.Println( "[SMARTCARD] card on reader",
					readerName,
					"is now disconnected" )

				// remove card from map
				delete( handler.cardList,
					readerName )
			}
		}
	}
}

// get last connected card
// if nil, no card was connected
func ( handler *Handler ) GetLastConnectedCard( ) *Card {
	if card, ok := handler.cardList[ handler.lastAddedCard ]; ok {
		handler.lastAddedCard = ""
		return card
	} else {
		return nil
	}
}
