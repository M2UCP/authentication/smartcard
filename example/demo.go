package main

import(
	"gitlab.com/M2UCP/authentication/smartcard"
	"fmt"
	"time"
)

func main( ) {
	// build card handler
	if handler, err := card.BuildHandler(); err == nil {
		for {
			// update
			handler.Update( )

			// get last connected card
			if c := handler.GetLastConnectedCard( ); c != nil {
				fmt.Println( "last connected card is on reader",
					c.GetReaderName( ) )
			}
			time.Sleep( time.Second )
		}
	} else {
		fmt.Println( err )
	}
}
