package card

import (
	"errors"
)

// mode
type CommandMode int
const(
	CommandModeRead = CommandMode( iota )
	CommandModeUpdate
	CommandModeVerify
)

// generate error from SW1/SW2
func GenerateError( SW1, SW2 uint8,
	mode CommandMode ) error {
	// handle errors
	if SW1 == 0x90 &&
		SW2 == 0x00 {
		return nil
	}

	switch mode {
	case CommandModeRead:
		if SW1 == 0x65 &&
			SW2 == 0x81 {
			return errors.New("unknown mode / PB_MEM")
		}
		if SW1 == 0x67 &&
			SW2 == 0x00 {
			return errors.New("invalid length of expected data / PB_LONG as described in Tables n° 8 & 9")
		}
		if SW1 == 0x69 &&
			SW2 == 0x82 {
			return errors.New("security not satisfied / PB_SECUR")
		}
		if SW1 == 0x6B &&
			SW2 == 0x00 {
			return errors.New("invalid P2 parameter / PB_PARAM")
		}
		if SW1 == 0x6D &&
			SW2 == 0x00 {
			return errors.New("invalid instruction byte (INS) / PB_INS")
		}
		break

	case CommandModeUpdate:
		if SW1 == 0x65 &&
			SW2 == 0x81 {
			return errors.New( "memory error: unknown flag, unknown mode or CTC reached maximum allowed value / PB_MEM" )
		}
		if SW1 == 0x67 &&
			SW2 == 0x00 {
			return errors.New( "invalid Lc value / PB_LONG (see tables 7 and 8)" )
		}
		if SW1 == 0x69 &&
			SW2 == 0x82 {
			return errors.New( "security not satisfied, words in balance updated in wrong order or attempt to update flag word / PB_SECUR")
		}
		if SW1 == 0x6B &&
			SW2 == 0x00 {
			return errors.New( "invalid P2 parameter / PB_PARAM" )
		}
		if SW1 == 0x6D &&
			SW2 == 0x00 {
			return errors.New( " invalid instruction byte (INS) / PB_INS" )
		}
		break

	case CommandModeVerify:
		if SW1 == 0x63 &&
			SW2 == 0x00 {
			return errors.New( "invalid Secret Code" )
		}
		if SW1 == 0x65 &&
			SW2 == 0x81 {
			return errors.New( "unknown mode" )
		}
		if SW1 == 0x67 &&
			SW2 == 0x00 {
			return errors.New( "invalid Lc value" )
		}
		if SW1 == 0x69 &&
			SW2 == 0x82 {
			return errors.New( "security not satisfied, maximum number of presentations exceeded" )
		}
		if SW1 == 0x6B &&
			SW2 == 0x00 {
			return errors.New( "invalid P2 parameter" )
		}
		if SW1 == 0x6D &&
			SW2 == 0x00 {
			return errors.New( "invalid instruction byte (INS)" )
		}
		break

	default:
		break
	}

	return errors.New( "unknown error" )
}
